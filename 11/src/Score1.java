import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Score {
	public static void main(String[] args) throws IOException {
		//设置变量放两个网页地址
		File smallHtml = new File("small.html");
		File allHtml = new File("all.html");
		// 使用InPutStream流读取properties文件
		Properties properties = new Properties();
		properties.load(new FileInputStream("total.properties"));
		// 配置文件取出元素
		double Before = Double.parseDouble(properties.getProperty("before"));
		// 算出课前测试的总共的分数
		double Base = Double.parseDouble(properties.getProperty("base"));
		// 算出课堂完成部分总共的分数
		double Test = Double.parseDouble(properties.getProperty("test"));
		// 算出小测部分的总共的分数
		double Program = Double.parseDouble(properties.getProperty("program"));
		// 算出编程题部分总共的分数
		double Add = Double.parseDouble(properties.getProperty("add"));
		HashMap<String, Integer> smallpart = getScores(smallHtml);
		HashMap<String, Integer> allpart = getScores(allHtml);				 
		double finalBefore=(smallpart.get("realBefore")+allpart.get("realBefore"))*0.25;
	    double finalBase=(smallpart.get("realBase")+allpart.get("realBase"))*0.3*0.95;
	    double finalTest=(smallpart.get("realTest")+allpart.get("realTest"))*0.2;
	    double finalProgram=(smallpart.get("realProgram")+allpart.get("realProgram"))*0.1;
	    double finalAdd =(smallpart.get("realAdd")+allpart.get("realAdd"))*0.05;
	    double sum=finalBefore+finalBase+finalTest+finalProgram+finalAdd;
		System.out.println(sum);
	}
	//获得云班课中获得的经验值
	private static HashMap<String, Integer> getScores(File file) throws IOException {
		// TODO Auto-generated method stub
		int realBefore =0;
		int realBase=0;
		int realTest=0;
		int realProgram=0;
		int realAdd=0;
		String name1 = "课堂自测";
		String name2 = "课堂完成部分";
		String name3 = "课堂小测";
		String name4 = "编程题";
		String name5 = "附加题";
		String exp_regex = "^(互评)?\\d+经验";
		int exp = 0;
		org.jsoup.nodes.Document document = Jsoup.parse(file, "utf-8");
		org.jsoup.select.Elements rootRow = document.getElementsByClass("interaction-row");
		for (int i = 0; i < rootRow.size(); i++) {			
			Elements rowChild = rootRow.get(i).children();
			Elements rowChild2 = rowChild.get(1).children();	
			Elements rowChild2_1 = rowChild2.get(0).children();
			Element section = rowChild2_1.get(1);
			Elements rowChild2_3 = rowChild2.get(2).children();
			Elements rowChild2_3_1 = rowChild2_3.get(0).children();
			Element span7 = rowChild2_3_1.get(rowChild2_3_1.size() - 2);
			// 已参与
			org.jsoup.nodes.Element span8 = rowChild2_3_1.get(rowChild2_3_1.size() - 1);
			// 经验值
			Pattern p = Pattern.compile(exp_regex);
			Matcher matcher = p.matcher(span8.text());
			boolean result = matcher.find();
			if (result) {
				exp = Integer.parseInt(matcher.group(0));
			}
			if (section.text().contains(name1)) {
				realBefore += exp;
			} else if (section.text().contains(name2)) {
				realBase += exp;
			} else if (section.text().contains(name3)) {
				realTest += exp;
			} else if (section.text().contains(name4)) {
				realProgram += exp;
			} else if (section.text().contains(name5)) {
				realAdd += exp;
			}
		}

		HashMap<String, Integer> sectionExperience = new HashMap<>();
		sectionExperience.put("realBefore", realBefore);
		sectionExperience.put("realBase", realBase);
		sectionExperience.put("realTest", realTest);
		sectionExperience.put("realProgram", realProgram);
		sectionExperience.put("realAdd", realAdd);

		return sectionExperience;
	}
}